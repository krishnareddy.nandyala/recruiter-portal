

import React, { Component } from 'react';
import SidemenuComponent from '../Sidemenu/index';

import HeaderComponent from '../Header/index';

import { APP_URLS } from '../../Common/Url';
import { countries } from '../../Common/country';
import '../../Components/Instant_Jobs/InstantJobs.scss';

export default class InstantJobsComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ProfileName: "",
            test: ""
        }
        this.retriveData()
    }

    retriveData =  async () => {
        const value = await localStorage.getItem('ProfileName');
        if (value !== null) {
        this.setState({ ProfileName: value })
        }

    }


    render() {
        // let  data  = this.state
        return (

            <div className="main">
                <HeaderComponent />
                <div className="row">
                    <div className="col-md-4">
                        <SidemenuComponent />
                    </div>
                    <div className="col-md-8">
                      Welcome  <p>{this.state.ProfileName}</p>
                    </div>
                </div>


            </div>
        )
    }
}

