

import React, { Component } from 'react';
import SidemenuComponent from '../Sidemenu/index';

import HeaderComponent from '../Header/index';

import { APP_URLS } from '../../Common/Url';
import { countries } from '../../Common/country';
import '../../Components/Signup/signup.scss';
import axios from 'axios';
import { ToastsContainer, ToastsContainerPosition, ToastsStore } from 'react-toasts';
import Modal from 'react-awesome-modal';



export default class SignupComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {

            countries: [],

            Name: '',
            mobilenumber: '',
            selectcode: '',

            selectcountryid: "",

            Email: "",
            Password: "",
            shortname: "",

            checkbox: true,

            visible: false,
            options: false,

           
        }

        this.getCountries()

    }

    getCountries = () => {
        let url = APP_URLS.getCountryDropDown;
        this.getCall(url).then(resp => {
            console.log("countries", resp);

            if (resp.status == 200) {
                this.setState({ countries: resp.data })
            }

        })
    }

    getCall = (url) => {
        return axios.get(url,
            {
                headers: {
                    'Content-Type': 'application/json'
                }
            }
        ).then((res) => {
            return res.data;
        })
            .catch(function (e) {
                console.log('ERROR ', e);
            })

    }


    signUp = () => {

        if (this.state.checkbox == false) {
            ToastsStore.warning("please accept terms and conditions")
            return;
        }

        let url = APP_URLS.REGISTER;

        let payload = {
            CountryId: this.state.selectcountryid,
            PhoneNumber: this.state.mobilenumber,
            Email: this.state.Email,
            Password: this.state.Password,
            CountryShortName: this.state.shortname
        }
        this.postCall(url, payload).then(res => {
            console.log("signup res", res);;
            if (res && res.status == 200) {
                ToastsStore.success(res.message)

                this.props.history.push('/');
            }
            else {
                ToastsStore.warning(res.message)
            }

        })

    }

    postCall = (url, payload) => {

        return axios.post(url,
            payload,
            {
                headers: {
                    'Content-Type': 'application/json',
                }
            }).then((res) => {
                console.log("login responce", res);
                return res.data;

            })
            .catch(function (e) {
                console.log('ERROR ', e);
            })
    }

    handleCheckBoxChange = (e) => {


        this.setState({ options: e.target.checked })

    }

    //select country

    selectCountry = (event) => {
        var val = event.target.value

        this.setState({ selectcountryid: val });

        this.state.countries.forEach(x => {
            if (x.CountryId == val) {
                this.setState({ shortname: x.ShortName })
            }
        })

    }

    openModal() {
        this.setState({
            visible: true
        });
    }

    closeModal() {
        this.setState({
            checkbox: false
        });


        this.setState({
            visible: false
        });
    }

    accept = () => {

        // this.state.checkbox = true;
        this.setState({
            options: true
        });
        this.setState({
            visible: false
        });
    }



    navToSignin = () => {


        this.props.history.push('/');
    }

    handleChangeChk = () => {

        this.setState({
            checkbox: !this.state.checkbox,
        });
        // this.setState({checkbox : true})
    }

    handleCheckBoxChange = (e) => {


        this.setState({ options: e.target.checked })

        console.log('aaaaaa:', this.state.options)
    }

   
    render() {

        const data = this.state

        var countryDropdown;


        countryDropdown = this.state.countries.map((item) => {
            return <option value={item.CountryId}>{item.CountryName}</option>
        })



        return (

          

            <div>


                <div className="row">
                    <div className="col-md-4">

                    </div>
                    <div className="col-md-4">

                        <ToastsContainer position={ToastsContainerPosition.TOP_RIGHT} store={ToastsStore} />
                        <div className="sign-in">Sign Up</div>
                        <div className="my-stytle">

                            <input type="test" className="input-styles" autoComplete="off"
                                id="exampleInputEmail1" aria-describedby="emailHelp"
                                placeholder="Enter Name"

                                name='name'
                                value={this.state.Name}
                                onChange={(e) => this.setState({ Name: e.target.value })}

                            />

                            <input type="test" className="input-styles" autoComplete="off"
                                id="exampleInputEmail1" aria-describedby="emailHelp"
                                placeholder="Enter email"

                                name='Email'
                                value={this.state.Email}
                                onChange={(e) => this.setState({ Email: e.target.value })}

                            />

                            <input type="number" className="input-styles" autoComplete="off"
                                id="exampleInputEmail1" aria-describedby="emailHelp"
                                placeholder="Mobile Number"

                                name='mobilenumber'
                                value={this.state.mobilenumber}
                                onChange={(e) => this.setState({ mobilenumber: e.target.value })}

                            />

                            {/* <div className="row" > */}
                            {/* <div className="col-md-4 dowpdown-styles"> */}


                            <select id="dropdown" className="select-styles" value={this.state.selectcountryid}
                                // onChange={(e) => this.setState({ selectcode: e.target.value })}
                                onChange={(e) => this.selectCountry(e)}
                            >

                                <option value="">Select country</option>

                                {countryDropdown}

                            </select>


                            {/* </div> */}


                            <br></br>



                            {/* <div className="col-md-8"> */}
                            <input type="password" autoComplete="off" className="input-styles"
                                id="exampleInputEmail1" aria-describedby="emailHelp"
                                placeholder="Enter Password"

                                name='Password'
                                value={this.state.Password}
                                onChange={(e) => this.setState({ Password: e.target.value })}

                            />
                            {/* </div> */}
                            {/* </div> */}


                            <input type="checkbox" name="vehicle1" value="Bike"
                                onChange={this.handleCheckBoxChange} />
                            <span>Please Accept <span className="terms" onClick={() => this.openModal()}>Terms and Conditions</span></span>

                            <br></br>


                           

                            <div >
                                <button type="button" className="signin-btn"
                                    onClick={this.signUp}
                                    disabled={this.state.options == false}
                                >SIGN Up</button>
                            </div>

                            <div>
                                <button type="button" className="signup-btn"
                                    onClick={this.navToSignin}
                                >SIGN-in</button>
                            </div>

                           

                        </div>

                    </div>
                    <div className="col-md-4">

                    </div>

                </div>

                <Modal visible={this.state.visible} width="400" height="300" effect="fadeInUp" onClickAway={() => this.closeModal()}>
                    <div className='text-center'>
                        <h1>Terms and conditions</h1>
                        <p>sjahfdlSFJfk'KF</p>
                        <p>sjahfdlSFJfk'KF</p>
                        <p>sjahfdlSFJfk'KF</p>
                        <p>sjahfdlSFJfk'KF</p>
                        <div className='row'>
                            <div className="col-md-6">
                                <a href="javascript:void(0);" onClick={this.accept} >Accept</a>
                            </div>
                            <div className="col-md-6">
                                <a href="javascript:void(0);" onClick={() => this.closeModal()}>Close</a>
                            </div>
                        </div>

                    </div>
                </Modal>


            </div>

        )
    }
}

// function mapStateToProps(state, actions) {
//     return state;
// }
// export default connect(mapStateToProps)(withRouter(SignInComponent));
