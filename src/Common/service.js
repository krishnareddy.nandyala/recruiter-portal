

import axios from 'axios';

export default class Service  {
   

    constructor() {
      
    }  

    commonGetCall = async(url,callback)=>{
        const token = await localStorage.getItem('token');

        if(token)
        {
            axios.get(url,
        {
            headers: {
                Authorization: token
            }
        }
    )
        .then((res) => {
            callback(res.data)
            // return res.data;
        })
        .catch((error) => {

            var obj = {
                status:400,
                message:error.message
            }

            callback(obj)
        })




        }
        else{

        }

    }

    commonPostMethod = async(url,payload,callback) =>
    {

        const token = await localStorage.getItem('token');

        if(token)
        {

                   axios.post(url,
                    payload,
            {
                headers: {
                    Authorization: token
                }
            }
        )
            .then((res) => {
                callback(res.data)
                
            })
            .catch((error) => {

                var obj = {
                    status:400,
                    message:error.message
                }

                callback(obj)
            })
            
        }else{

        }

    }




   



}