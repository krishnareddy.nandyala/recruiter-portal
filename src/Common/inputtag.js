import React, { Component } from 'react';

export default class InputTagComponet extends Component {


   
    render() {
        const { value,placeholder, inputtype, ...props } = this.props;
        

        return (

            // <input
            //     {...props}
            //     style={{ height: 36, fontSize: 14, color: '#000', paddingBottom: 3, borderBottomWidth: 1, borderBottomColor: 'lightgray' }}
            //     onFocus={this.handleFocus}
            //     onBlur={this.handleBlur}
            //     blurOnSubmit
            // />
            <div>
                <input  {...props} autoComplete="off"
                    id="exampleInputEmail1" aria-describedby="emailHelp"
                    placeholder={placeholder}
                    value={value}
                    type={inputtype}
                />

            </div>
         
     
    );
    }
}
