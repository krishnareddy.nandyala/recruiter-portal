import { SIGNIN } from './action.type';

let initialState = {
  
    token: '',
    isLoggedIn: '',
    loginError:"",
   
}

export const LoginApp = (state=initialState, action) => {
    switch (action.type) {
        case SIGNIN.LOGIN_SUCCESS:

        console.log("my data is " , action.data.data );
        
            return {
                ...state, isLoggedIn: true,
                token: action.data.data.token
            }
           
        case SIGNIN.LOGIN_FAILURE :
            return {...state,loginError:'Login Failure'}
           
        default:
            return {...state};
    }

}