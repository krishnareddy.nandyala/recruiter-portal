
import axios from 'axios';

import { SIGNIN } from './action.type';

export const Login = (url, loginObj) => {
    return dispatch => {

        axios.post(url, loginObj)
            .then(result => {
                console.log(result);
                if (result.data.status == 200) {

                    dispatch(LoginSuccess(result.data));
                }
                if (result.data.status == 400) {

                    dispatch(LoginFaliure(result.data));
                }


            })
            .catch(err => {

                dispatch(LoginFaliure());
                console.log(err);
            });
    };
}


export const LoginSuccess = (result) => {
    return {
        type: SIGNIN.LOGIN_SUCCESS,
        data: result
    }
}
export const LoginFaliure = (result) => {
    return {
        type: SIGNIN.LOGIN_FAILURE,
        data: result
    }
}